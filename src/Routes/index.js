import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "../Pages/Login";
import Home from "../Pages/Home";
import Company from "../Pages/Company";
const ROUTES = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={<Home />} />
        <Route path="/enterprise/:paramId" element={<Company />} />
      </Routes>
    </Router>
  );
};
export default ROUTES;
