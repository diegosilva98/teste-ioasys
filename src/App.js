import ROUTES from "./Routes";

function App() {
  return (
    <div className="app-container">
      <ROUTES />
    </div>
  );
}

export default App;
