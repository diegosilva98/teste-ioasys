import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import "./index.css";
import logoHome from "../../assets/logo-home@2x.png";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { useHeaders } from "../../Providers/loginProvider";

const schema = yup
  .object()
  .shape({
    email: yup.string().required("Required").email(),
    password: yup.string().required("Required"),
  })
  .required();

const Login = () => {
  const { getHeaders, setToLocalStorage } = useHeaders();
  const navigate = useNavigate();
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const [error, setError] = useState(false);

  const postLogin = async (data) => {
    try {
      const response = await axios.post(
        "https://empresas.ioasys.com.br/api/v1/users/auth/sign_in",
        data
      );
      console.log(response.headers);
      getHeaders(response.headers);
      setToLocalStorage(response);
      setError(false);
      reset();
      navigate("/home");
    } catch (error) {
      console.error(error);
      setError(true);
    }
  };

  return (
    <div className="page-login-container">
      <div className="form-container">
        <div className="icon-container">
          <img className="icon-logo-home" alt="logo-home" src={logoHome} />
        </div>
        <div className="text-home-container">
          <p className="text-wellcome">
            BEM-VINDO AO <br></br>EMPRESAS
          </p>
          <p className="text-home">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod
          </p>
        </div>
        <form
          onSubmit={handleSubmit(postLogin)}
          className="form-content-container"
        >
          <input
            className="input-login"
            {...register("email")}
            type="text"
            id="Email"
            name="email"
            placeholder="Email"
            value="testeapple@ioasys.com.br"
          />
          <input
            className="input-password"
            {...register("password")}
            type="password"
            id="Senha"
            name="password"
            placeholder={"Senha"}
            value="12341234"
          />
          {errors.email || errors.password || error ? (
            <span className="error-message">
              Credenciais informadas são inválidas, tente novamente
            </span>
          ) : (
            <span className="error-message" />
          )}
          <input
            className="input-submit-login"
            type="submit"
            value="ENTRAR"
          ></input>
        </form>
      </div>
    </div>
  );
};
export default Login;
