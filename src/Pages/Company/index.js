import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import { useEnterprises } from "../../Providers/enterprisesProvider";
import logoHeader from "../../assets/logo-nav.png";
// // import { useState } from "react";
import "./index.css";

const Company = () => {
  const { enterprises } = useEnterprises();
  const { paramId } = useParams();
  let enterprise = null;
  enterprises ? (enterprise = enterprises[paramId]) : (enterprise = null);

  const url = "https://empresas.ioasys.com.br";
  return (
    <div className="page-company-container">
      <header className="header-container">
        <div className="header-content-container">
          <div className="logo-header-contaienr">
            <img alt="logo-header" src={logoHeader} />
          </div>
        </div>
      </header>
      {enterprise ? (
        <div className="company-component-com">
          <img
            className="company-image-com"
            alt="companyImage"
            src={url + enterprise.photo}
          />
          <div className="name-description-container">
            <p className="enterprise-type">{enterprise.description}</p>
          </div>
        </div>
      ) : null}
      <Link to="/home">Voltar</Link>
    </div>
  );
};

export default Company;
