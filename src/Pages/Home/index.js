import "./index.css";
import logoHeader from "../../assets/logo-nav.png";
import axios from "axios";
import { useEffect, useState } from "react";

import { Link } from "react-router-dom";
// import { useHeaders } from "../../Providers/loginProvider";
// import { useEnterprises } from "../../Providers/enterprisesProvider";

const Home = () => {
  const [enterprises, setEnterprises] = useState();
  const [terms, setTerms] = useState("");

  const token_access = localStorage.getItem("access-token");
  const client = localStorage.getItem("client");
  const uid = localStorage.getItem("uid");

  const url = "https://empresas.ioasys.com.br";

  const getCompanyes = async (terms) => {
    try {
      const response = await axios.get(
        `https://empresas.ioasys.com.br/api/v1/enterprises?name=${terms}`,
        {
          headers: {
            "access-token": token_access,
            client: client,
            uid: uid,
          },
        }
      );
      setEnterprises(response.data.enterprises);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    getCompanyes(terms);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [terms]);

  return (
    <div className="page-home-container">
      <header className="header-container">
        <div className="header-content-container">
          <div className="logo-header-contaienr">
            <img alt="logo-header" src={logoHeader} />
          </div>
        </div>
        <div>
          <input
            className="input-search"
            type="text"
            id="Search"
            name="search"
            placeholder="Pesquisar"
            onChange={(e) => {
              setTerms(e.target.value);
            }}
          />
        </div>
      </header>
      <div className="main-conainer">
        {terms ? (
          enterprises.map((enterprise, index) => {
            return (
              <div key={index} className="company-component">
                <img
                  className="company-image"
                  alt="companyImage"
                  src={url + enterprise.photo}
                />
                <div className="name-description-container">
                  <Link to={`/enterprise/` + enterprise.id}>
                    <h2 className="enterprise-name">
                      {enterprise.enterprise_name}
                    </h2>
                  </Link>
                  <p className="enterprise-type">
                    {enterprise.enterprise_type.enterprise_type_name}
                  </p>
                  <p className="enterprise-country">{enterprise.country}</p>
                </div>
              </div>
            );
          })
        ) : (
          <p className="message">Clique na busca para iniciar.</p>
        )}
      </div>
    </div>
  );
};

export default Home;
