import { HeadersLoginProvider } from "./loginProvider";
import { EnterprisesProvider } from "./enterprisesProvider";

const Provider = ({ children }) => {
  return (
    <HeadersLoginProvider>
      <EnterprisesProvider>{children}</EnterprisesProvider>
    </HeadersLoginProvider>
  );
};
export default Provider;
