// import { useEffect } from "react";
import { createContext, useContext, useState } from "react";

const headersContext = createContext();

export const HeadersLoginProvider = ({ children }) => {
  const [headers, setHeaders] = useState();

  const getHeaders = (data) => {
    setHeaders(data);
  };
  const setToLocalStorage = (data) => {
    localStorage.setItem("access-token", data.headers["access-token"]);
    localStorage.setItem("client", data.headers["client"]);
    localStorage.setItem("uid", data.headers["uid"]);
  };

  return (
    <headersContext.Provider value={{ headers, getHeaders, setToLocalStorage }}>
      {children}
    </headersContext.Provider>
  );
};

export const useHeaders = () => useContext(headersContext);
