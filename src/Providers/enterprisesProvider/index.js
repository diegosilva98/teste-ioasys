// import { useEffect } from "react";
import { createContext, useContext, useEffect, useState } from "react";

import axios from "axios";

const enterpriseContext = createContext();

export const EnterprisesProvider = ({ children }) => {
  const [enterprises, setEnterprises] = useState();

  const getEnterprises = (data) => {
    setEnterprises(data);
  };

  const token_access = localStorage.getItem("access-token");
  const client = localStorage.getItem("client");
  const uid = localStorage.getItem("uid");

  const getAllCompanyes = async () => {
    try {
      const response = await axios.get(
        `https://empresas.ioasys.com.br/api/v1/enterprises`,
        {
          headers: {
            "access-token": token_access,
            client: client,
            uid: uid,
          },
        }
      );
      setEnterprises(response.data.enterprises);
    } catch (error) {
      console.error(error);
    }
  };
  useEffect(() => {
    getAllCompanyes();
    console.log("rodou");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  console.log(enterprises);
  return (
    <enterpriseContext.Provider value={{ enterprises, getEnterprises }}>
      {children}
    </enterpriseContext.Provider>
  );
};

export const useEnterprises = () => useContext(enterpriseContext);
